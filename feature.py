# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013 E-MIPS (http://www.e-mips.com.ar) All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class project_feature(osv.osv):
    _name = 'project.feature'
    _description = 'Project Feature'
    _order = 'sequence'

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'name': fields.char('Summary', required=True, size=128),
        'sequence': fields.integer('#', select=True, help="Gives the sequence order and a unique identification."),
        'description': fields.text('Description'),
        # TODO: Agregar campos de time remaining y time spent pero calculado por medio de las tasks de cada story
        'story_ids': fields.one2many('project.scrum.user.story', 'feature_id', "User Stories"),

        # TODO: Ver si hace falta agregar un campo para asignar gente de Soporte. O directamente, poder elegir gente y asignarles
        # un rol en particular, como puede ser, desarrollador, soporte, QA, etc.
        #'supporter_ids': fields.many2many('res.users', 'project_user_rel', 'project_id', 'uid', 'Project Members',
            #help="Project's members are users who can have an access to the tasks related to this project.", states={'close':[('readonly',True)], 'cancelled':[('readonly',True)]}),

        # TODO: Agregar la relacion con un Release (project.scrum.release)
        'release_id': fields.many2one('project.scrum.release', 'Release'),
        'project_id': fields.many2one('project.project', 'Project', required=True),
        'categ_ids': fields.many2many('project.category', string='Tags'),
        'importance': fields.selection([('nice','Nice to Have'),('must','Must Have'),('normal', 'Normal')], 'Importance'),
        'date_start': fields.date('Planned Start',select=True),
        'date_end': fields.date('Planned End',select=True),
        'state': fields.selection([('draft','New'),('open','In Progress'),('cancelled', 'Cancelled'),('done','Done')], 'Status', required=True),
    }

    _defaults = {
        'state': 'draft', 
    }

project_feature()
