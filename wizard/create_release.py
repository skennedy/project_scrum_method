# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013 E-MIPS (http://www.e-mips.com.ar) All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from tools.translate import _

class project_create_release(osv.osv_memory):
    _name = 'project.create.release'
    _description = 'Project Scrum Create Release'

    _columns = {
        'name': fields.char('Release Name', size=64, required=True, help="New title of the release to be created"),
        'project_id': fields.many2one('project.project', 'Project', required=True),
        'story_ids': fields.many2many('project.scrum.user.story', 'project_user_story_release', 'wizard_id', 'story_id'),
        }

    def view_init(self, cr, uid, fields_list, context=None):
        user_story_obj = self.pool.get('project.scrum.user.story')

        if context is None:
            context = {}
        project_id = False
        if context.get('active_model', '') == 'project.scrum.user.story':
            for story in user_story_obj.browse(cr, uid, context['active_ids'], context=context):
                if not project_id:
                    project_id = story.project_id.id
                else:
                    if project_id != story.project_id.id:
                        raise osv.except_osv(_('Error!'), _('You cannot create Release with User Stories from different Projects.'))
            pass

    def default_get(self, cr, uid, fields, context=None):
        """
        This function gets default values
        """
        res = super(project_create_release, self).default_get(cr, uid, fields, context=context)
        if context is None:
            context = {}

        record_id = context and context.get('active_id', False) or False
        if not record_id:
            return res

        user_story_obj = self.pool.get('project.scrum.user.story')

        story_ids = []
        if context.get('active_model', '') == 'project.scrum.user.story':
            story_read = user_story_obj.read(cr, uid, context['active_ids'], ['project_id', 'release_id'], context=context)
            project_id = story_read[0]['project_id'][0]

            #for story in story_reads:
                #story_ids.append()
            story_ids = [story['id'] for story in story_read if story['release_id'] == False]
        else:
            project_id = record_id

        if 'project_id' in fields:
            res['project_id'] = project_id if project_id else False

        if 'story_ids' in fields:
            res['story_ids'] = story_ids

        return res

    def onchange_project_id(self, cr, uid, ids, project_id, context=None):
        res = {}

        if not project_id:
            return res

        domain = [('project_id','=',project_id), ('release_id','=',False), ('sprint_id','=',False)]
        res['domain'] = {'story_ids': domain}

        return res

    def create_release(self, cr, uid, ids, context=None):

        release_obj = self.pool.get('project.scrum.release')
        feature_obj = self.pool.get('project.feature')
        issue_obj = self.pool.get('project.issue')
        story_obj = self.pool.get('project.scrum.user.story')

        data = self.browse(cr, uid, ids[0], context)

        release_vals = {
            'name': data.name,
            'project_id': data.project_id.id,
        }

        release_id = release_obj.create(cr, uid, release_vals, context=context)

        release_stories = []
        release_features = []
        release_issues = []

        for story in data.story_ids:
            release_stories.append(story.id)
            if story.feature_id:
                release_features.append(story.feature_id.id)

            if story.issue_ids:
                release_issues += [issue.id for issue in story.issue_ids]

        story_obj.write(cr, uid, release_stories, {'release_id': release_id, 'state': 'open'}, context=context)
        feature_obj.write(cr, uid, release_features, {'release_id': release_id}, context=context)
        issue_obj.write(cr, uid, release_issues, {'release_id': release_id}, context=context)


project_create_release()
