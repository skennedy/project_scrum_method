# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013 E-MIPS (http://www.e-mips.com.ar) All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Project Scrum Method',
    'version': '1.0',
    'author': 'E-MIPS',
    'website': 'http://www.e-mips.com.ar',
    'category': 'Project Management',
    'sequence': 8,
    'summary': 'Projects, Tasks',
    'images': [
    ],
    'depends': [
        'base_setup',
        'base_status',
        'product',
        'project',
        'project_issue',
        #'analytic',
        'board',
        'mail',
        'resource',
        'web_kanban'
    ],
    'description': """
Track projects using Scrum Method
    """,
    'data': [
        #'security/project_security.xml',
        #'board_project_view.xml',
        #'res_config_view.xml',
        'wizard/create_release_view.xml',
        'wizard/create_sprint_view.xml',
        'project_scrum_view.xml',
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'css': [],
    'js': [],
}
#
